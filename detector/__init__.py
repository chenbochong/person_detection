from .profile_person_detector import ProfilePersonDetector
from .roi_person_detector import RoiPersonDetector

__all__ = ['ProfilePersonDetector', 'RoiPersonDetector']