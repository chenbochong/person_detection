import torch
import numpy as np
import cv2
import math

from model import RoiNet


class RoiPersonDetector:
    def __init__(self, weight_path, class_names=None, strides=None, model_divisor=32,
                 conf_threshold=0.6, nms_threshold=0.45, top_k=5000):
        self.class_names = ['Person', 'Vehicle'] if class_names is None else class_names
        self.num_class = len(self.class_names)
        self.strides = [8, 16, 32] if strides is None else strides
        self.model_divisor = model_divisor
        self.conf_threshold = conf_threshold
        self.nms_threshold = nms_threshold
        self.top_k = top_k
        self.is_onnx = weight_path.endswith('.onnx')
        if self.is_onnx:
            self.net = cv2.dnn.readNet(weight_path)
            self.output_names = ['cls_8', 'cls_16', 'cls_32', 'bbox_8', 'bbox_16', 'bbox_32', 'obj_8', 'obj_16', 'obj_32']
        else:
            backbone = dict(
                stage_channels=[[3, 16, 16], [16, 64], [64, 64], [64, 64], [64, 64], [64, 64]],
                downsample_idx=[0, 2, 3, 4],
                out_idx=[3, 4, 5]
            )
            neck = dict(
                in_channels=[64, 64, 64],
                out_idx=[0, 1, 2]
            )
            bbox_head = dict(
                num_classes=self.num_class,
                in_channels=64,
                shared_stacked_convs=1,
                stacked_convs=0,
                feat_channels=64,
                strides=[8, 16, 32]
            )
            self.model = RoiNet(backbone, neck, bbox_head)
            state_dict = torch.load(weight_path, map_location=torch.device('cpu'))['state_dict']
            self.model.load_state_dict(state_dict, strict=False)
            self.model.eval()

    def pad_image(self, img):
        assign_h = math.ceil(img.shape[0] / self.model_divisor) * self.model_divisor
        assign_w = math.ceil(img.shape[1] / self.model_divisor) * self.model_divisor
        if assign_h == img.shape[0] and assign_w == img.shape[1]:
            return img
        return np.pad(img, ((0, assign_h - img.shape[0]), (0, assign_w - img.shape[1])))

    def detect(self, img):
        img = self.pad_image(img)
        if self.is_onnx:
            img_blob = cv2.dnn.blobFromImage(img)
            self.net.setInput(img_blob)
            outputs = self.net.forwardAndRetrieve(self.output_names)
            outputs = [output[0] for output in outputs]
            cls_preds, bbox_preds, obj_preds = outputs[0:3], outputs[3:6], outputs[6:9]
        else:
            with torch.no_grad():
                img_tensor = torch.from_numpy(img).permute(2, 0, 1).unsqueeze(0).to(torch.float32)
                cls_preds, bbox_preds, obj_preds = self.model(img_tensor)
        bboxes = [[] for _ in range(self.num_class)]
        scores = [[] for _ in range(self.num_class)]
        for cls, bbox, obj, stride in zip(cls_preds, bbox_preds, obj_preds, self.strides):
            if not self.is_onnx:
                cls, bbox, obj = cls.numpy(), bbox.numpy(), obj.numpy()
            confs = np.sqrt(cls[0].clip(0, 1) * obj[0].clip(0, 1))
            ind_cls, ind_y, ind_x = np.where(confs > self.conf_threshold)
            if len(ind_y) == 0:
                continue
            score = confs[ind_cls, ind_y, ind_x]
            ws = np.exp(bbox[0, 2, ind_y, ind_x]) * stride
            hs = np.exp(bbox[0, 3, ind_y, ind_x]) * stride
            xs = (bbox[0, 0, ind_y, ind_x] + ind_x) * stride - (ws / 2)
            ys = (bbox[0, 1, ind_y, ind_x] + ind_y) * stride - (hs / 2)
            for x, y, w, h, c, s in zip(xs, ys, ws, hs, ind_cls, score):
                bboxes[c].append(np.array([x, y, w, h]).astype(int))
                scores[c].append(s)
        results = []
        for i in range(self.num_class):
            indices = cv2.dnn.NMSBoxes(bboxes[i], scores[i], self.conf_threshold, self.nms_threshold, 1.0, self.top_k)
            for j in indices:
                results.append(dict(bbox=bboxes[i][j], score=scores[i][j], cls=i, cls_name=self.class_names[i]))
        return results
