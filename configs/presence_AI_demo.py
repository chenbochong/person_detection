from .base_demo import *

demo_config = [
    dict(
        key='1',
        type='PersonDetProfileDemo',
        desc='Person Detection-Oculi Visual AI Mode',
        kwargs=dict(
            weight_path='onnx/profile_net.onnx'
        ),
        actions=dict(
            q=dict(
                action='switch_display_mode',
                args=('black',)
            ),
            w=dict(
                action='switch_display_mode',
                args=('event_image',)
            ),
            e=dict(
                action='switch_display_mode',
                args=('col_profile',)
            ),
            r=dict(
                action='switch_display_mode',
                args=('actionable_signal',)
            )
        )
    )
]
