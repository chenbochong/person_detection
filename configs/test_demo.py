from .base_demo import *

demo_config = [
    dict(
        key='1',
        type='ForegroundExtractionDemo2',
        desc='Presence Detection-OCULI SPU Vision Mode',
        kwargs=dict(),
        actions=dict(
            r=dict(
                action='reset_background',
                args=()
            )
        )
    )
]
