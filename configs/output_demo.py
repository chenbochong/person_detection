from .base_demo import *

power_meter_enabled = True
power_meter_static_watts = 3.5
power_meter_address = '98:DA:F0:00:59:D6'
demo_config = [
    dict(
        key='1',
        type='FullFrameDemo',
        desc='Full Frame',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='2',
        type='MotionEventDemo',
        desc='Oculi Smart Events-Motion',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='3',
        type='ColorEventDemo',
        desc='Oculi Smart Events-Color',
        kwargs=dict(),
        actions=dict(
            q=dict(
                action='set_threshold',
                args=(0,)
            ),
            w=dict(
                action='set_threshold',
                args=(1,)
            ),
        )
    ),
    dict(
        key='4',
        type='ActionableSignalDemo',
        desc='Actionable Signal',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='5',
        type='RandomAccessDemo',
        desc='Random Access',
        kwargs=dict(),
        actions=dict(
            q=dict(
                action='switch_display_mode',
                args=('rows_x_cols',)
            ),
            w=dict(
                action='switch_display_mode',
                args=('cols',)
            ),
        )
    ),
    dict(
        key='6',
        type='FaceDetFullFrameDemo',
        desc='Face Detection-Imaging Mode',
        kwargs=dict(
            weight_path='onnx/yunet_face.onnx'
        ),
        actions=dict()
    ),
    dict(
        key='7',
        type='FaceDetRoiDemo',
        desc='Face Detection-OCULI SPU Vision Mode',
        kwargs=dict(
            weight_path='onnx/yunet_face.onnx'
        ),
        actions=dict()
    )
]
