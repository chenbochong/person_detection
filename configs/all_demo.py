from .base_demo import *

demo_config = [
    dict(
        key='1',
        type='FullFrameDemo',
        desc='Full Frame',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='2',
        type='MotionEventDemo',
        desc='Oculi Smart Events-Motion',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='3',
        type='ColorEventDemo',
        desc='Oculi Smart Events-Color',
        kwargs=dict(),
        actions=dict(
            q=dict(
                action='set_threshold',
                args=(0,)
            ),
            w=dict(
                action='set_threshold',
                args=(1,)
            ),
        )
    ),
    dict(
        key='4',
        type='ActionableSignalDemo',
        desc='Actionable Signal',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='5',
        type='RandomAccessDemo',
        desc='Random Access',
        kwargs=dict(),
        actions=dict(
            q=dict(
                action='switch_display_mode',
                args=('rows_x_cols',)
            ),
            w=dict(
                action='switch_display_mode',
                args=('cols',)
            ),
        )
    ),
    dict(
        key='6',
        type='FaceDetFullFrameDemo',
        desc='Face Detection-Imaging Mode',
        kwargs=dict(
            weight_path='onnx/yunet_face.onnx'
        ),
        actions=dict()
    ),
    dict(
        key='7',
        type='PersonDetProfileDemo',
        desc='Person Detection-Oculi Visual AI Mode',
        kwargs=dict(
            weight_path='onnx/profile_net.onnx'
        ),
        actions=dict(
            q=dict(
                action='switch_display_mode',
                args=('black',)
            ),
            w=dict(
                action='switch_display_mode',
                args=('event_image',)
            ),
            e=dict(
                action='switch_display_mode',
                args=('col_profile',)
            ),
            r=dict(
                action='switch_display_mode',
                args=('actionable_signal',)
            )
        )
    ),
    dict(
        key='8',
        type='UsefulSensorsDemo',
        desc='Useful Sensors Face Detection',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='9',
        type='PIRDemo',
        desc='PIR Presence Detection',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='0',
        type='ForegroundExtractionDemo',
        desc='Presence Detection-OCULI SPU Vision Mode',
        kwargs=dict(),
        actions=dict(
            r=dict(
                action='reset_background',
                args=()
            )
        )
    )
]
