from .base_demo import *

power_meter_enabled = True
power_meter_static_watts = 3.5
power_meter_address = '98:DA:F0:00:59:D6'
demo_config = [
    dict(
        key='1',
        type='FaceDetFullFrameDemo',
        desc='Face Detection-Imaging Mode',
        kwargs=dict(
            weight_path='onnx/yunet_face.onnx'
        ),
        actions=dict()
    ),
    dict(
        key='2',
        type='FaceDetRoiDemo',
        desc='Face Detection-OCULI SPU Vision Mode',
        kwargs=dict(
            weight_path='onnx/yunet_face.onnx'
        ),
        actions=dict()
    )
]
