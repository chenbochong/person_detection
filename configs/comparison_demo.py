from .base_demo import *

demo_config = [
    dict(
        key='1',
        type='UsefulSensorsDemo',
        desc='Useful Sensors Face Detection',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='2',
        type='PIRDemo',
        desc='PIR Presence Detection',
        kwargs=dict(),
        actions=dict()
    ),
    dict(
        key='3',
        type='ForegroundExtractionDemo',
        desc='Presence Detection-OCULI SPU Vision Mode',
        kwargs=dict(),
        actions=dict(
            r=dict(
                action='reset_background',
                args=()
            )
        )
    )
]
