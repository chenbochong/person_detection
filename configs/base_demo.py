freq_limit_ms = 13
resolution = (640, 480)
power_meter_enabled = False
power_meter_static_watts = 0.0
power_meter_address = None
playback_config = dict(
    playback_enabled=True,
    playback_duration=5
)
emulator_config = dict(
    use_picam=True,
    motion_threshold=25,
    picam_config=dict(
        ae_enable=False,
        af_mode=False,
        lens_position=0.0,
        awb_enable=False,
        analog_gain=5.0,
        exposure_us=8000
    )
)
demo_config = [
    dict(
        key='1',
        type='FullFrameDemo',
        desc='Full Frame',
        kwargs=dict(),
        actions=dict()
    )
]
