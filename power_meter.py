from bluetooth import *
import collections
import datetime
from threading import Thread


def process_data(d):
    data = {}

    data["Volts"] = struct.unpack(">h", d[2: 3 + 1])[0] / 1000.0  # volts
    data["Amps"] = struct.unpack(">h", d[4: 5 + 1])[0] / 10000.0  # amps
    data["Watts"] = struct.unpack(">I", d[6: 9 + 1])[0] / 1000.0  # watts
    data["temp_C"] = struct.unpack(">h", d[10: 11 + 1])[0]  # temp in C
    data["temp_F"] = struct.unpack(">h", d[12: 13 + 1])[0]  # temp in F

    utc_dt = datetime.datetime.now(datetime.timezone.utc)  # UTC time
    dt = utc_dt.astimezone()  # local time
    data["time"] = dt

    g = 0
    for i in range(16, 95, 8):
        ma, mw = struct.unpack(">II", d[i: i + 8])  # mAh,mWh respectively
        gs = str(g)
        data[gs + "_mAh"] = ma
        data[gs + "_mWh"] = mw
        g += 1

    data["data_line_pos_volt"] = struct.unpack(">h", d[96: 97 + 1])[0] / 100.0
    data["data_line_neg_volt"] = struct.unpack(">h", d[98: 99 + 1])[0] / 100.0
    data["resistance"] = struct.unpack(">I", d[122: 125 + 1])[0] / 10.0  # resistance
    return data


class PowerMeter:

    def __init__(self, addr=None, static_watts=0.0, leng=20):
        self.volts = collections.deque(maxlen=leng)
        self.currents = collections.deque(maxlen=leng)
        self.watts = collections.deque(maxlen=leng)
        self.times = collections.deque(maxlen=leng)
        self.static_watts = static_watts
        self.dynamic_watts = 0.0
        self.stopped = True
        self.thread = None
        if addr is not None:
            self.addr = addr
            self.sock = BluetoothSocket(RFCOMM)
            self.sock.connect((self.addr, 1))
        else:
            nearby_devices = discover_devices(lookup_names=True)
            for v in nearby_devices:
                if "UM25C" in v[1]:
                    print("Found", v[0])
                    self.addr = v[0]
                    break
            if self.addr is None:
                raise RuntimeError('No address provided')
            service_matches = find_service(address=self.addr)
            if len(service_matches) == 0:
                raise RuntimeError('No services found for address')
            first_match = service_matches[0]
            port = first_match["port"]
            name = first_match["name"]
            host = first_match["host"]
            if host is None or port is None:
                raise RuntimeError('Host or port not specified')
            self.sock = BluetoothSocket(RFCOMM)
            print(f'connecting to "{name}" on {host}:{port}')
            self.sock.connect((host, port))

    def start(self):
        if not self.stopped:
            return
        self.stopped = False
        self.thread = Thread(target=self.update, args=())
        self.thread.start()

    def stop(self):
        self.stopped = True
        if self.thread is not None:
            self.thread.join()

    def release(self):
        self.stop()
        self.sock.close()

    def update(self):
        d = b""
        while True:
            if self.stopped:
                return
            # Send request to USB meter
            self.sock.send((0xF0).to_bytes(1, byteorder="big"))

            d += self.sock.recv(130)

            if len(d) != 130:
                continue

            data = process_data(d)
            self.volts.append(data["Volts"])
            self.currents.append(data["Amps"])
            self.watts.append(data["Watts"])
            self.times.append(data["time"])

            d = b""

            # Send message over ZMQ (inference time in milliseconds)
            self.dynamic_watts = (data["Watts"] - self.static_watts)  # Watts

    def read_dynamic_watts(self):
        return self.dynamic_watts
