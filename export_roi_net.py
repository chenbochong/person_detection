import torch

from model import RoiNet

weight_path = 'weights/roi_person_det.pth'
backbone = dict(
    stage_channels=[[3, 16, 16], [16, 64], [64, 64], [64, 64], [64, 64], [64, 64]],
    downsample_idx=[0, 2, 3, 4],
    out_idx=[3, 4, 5]
)
neck = dict(
    in_channels=[64, 64, 64],
    out_idx=[0, 1, 2]
)
bbox_head = dict(
    num_classes=1,
    in_channels=64,
    shared_stacked_convs=1,
    stacked_convs=0,
    feat_channels=64,
    strides=[8, 16, 32]
)

torch_model = RoiNet(backbone, neck, bbox_head)
state_dict = torch.load(weight_path, map_location=torch.device('cpu'))['state_dict']
torch_model.load_state_dict(state_dict, strict=False)
img = torch.randn(1, 3, 640, 640)

torch.onnx.export(torch_model,
                  args=img,
                  f='onnx/roi_net.onnx',
                  input_names=['img'],
                  output_names=['cls_8', 'cls_16', 'cls_32', 'bbox_8', 'bbox_16', 'bbox_32', 'obj_8', 'obj_16', 'obj_32'])

