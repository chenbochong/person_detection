import cv2

COLOR_PRED = (0, 0, 255)

FONT = cv2.FONT_HERSHEY_SIMPLEX
FONT_SCALE = 0.5
FONT_THICKNESS = 1

# Deep Gaze: https://github.com/mpatacchiola/deepgaze
DEEP_GAZE_LOWER_HSV = (0, 58, 50)    # lower bound skin HSV
DEEP_GAZE_UPPER_HSV = (30, 255, 255) # upper bound skin HSV

# Two range approach. This can also be seen in our 300VW dataset.
# https://stackoverflow.com/questions/8753833/exact-skin-color-hsv-range
# 1st range of Hue
RANGE1_LOWER_HSV = (0, 30, 53)
RANGE1_UPPER_HSV = (20, 150, 255)

# 2nd range of Hue (OpenCV converts 360 to 180)
RANGE2_LOWER_HSV = (172, 30, 53)
RANGE2_UPPER_HSV = (180, 180, 210)

# YCrCb limits: https://www.alliedacademies.org/articles/enhanced-skin-tone-detection-using-heuristic-thresholding.html
LOWER_YCrCb_0 = (0, 133, 77)
UPPER_YCrCb_0 = (255, 173, 120)

# YCrCb limits: https://www.sciencedirect.com/science/article/pii/S1877050915018918
LOWER_YCrCb_1 = (0, 150, 100)
UPPER_YCrCb_1 = (255, 200, 150)