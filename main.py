import argparse
import importlib
import cv2
import timeit

from spu_emulator import SpuEmulator
import demo_processor

import os

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"


def parse_args():
    parser = argparse.ArgumentParser(description='Oculi® Live Demo')
    parser.add_argument('config', help='Configuration file name (.py file under /configs)')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    config = {k: v
              for k, v in vars(importlib.import_module(f'configs.{args.config}')).items()
              if not k.startswith('__')}
    win_name = 'Oculi® Live Demo'

    resolution = config['resolution']
    emulator = SpuEmulator(resolution, **config['emulator_config'])
    emulator.start()
    power_meter = None
    if config['power_meter_enabled']:
        try:
            from power_meter import PowerMeter
            power_meter = PowerMeter(config['power_meter_address'], config['power_meter_static_watts'])
            power_meter.start()
        except Exception as e:
            print(e)
    cv2.namedWindow(win_name, cv2.WINDOW_NORMAL)

    demo_key_list = []
    processor_dict = {}
    desc_dict = {}
    actions_dict = {}
    for demo_config in config['demo_config']:
        demo_key = demo_config['key']
        demo_key_list.append(demo_key)
        processor_dict[demo_key] = getattr(demo_processor, demo_config['type'])(emulator, power_meter,
                                                                                **demo_config['kwargs'])
        desc_dict[demo_key] = demo_config['desc']
        actions_dict[demo_key] = demo_config['actions']

    playback_config = config['playback_config']
    playback_enabled = playback_config['playback_enabled']
    playback_duration = playback_config['playback_duration']
    curr_idx = 0
    curr_key = demo_key_list[curr_idx]
    processor_dict[curr_key].start()

    playback_start = timeit.default_timer()
    try:
        while True:
            curr_processor = processor_dict[curr_key]
            curr_actions = actions_dict[curr_key]
            curr_desc = desc_dict[curr_key]
            start = timeit.default_timer()
            output = curr_processor.compute_output_frame()

            # limit frequency
            key_wait_time = max(1, config['freq_limit_ms'] - int((timeit.default_timer() - start) * 1000))
            key_down = cv2.waitKey(key_wait_time)

            cv2.putText(output, curr_desc, (10, 30), cv2.FONT_HERSHEY_DUPLEX, 0.8, (126, 0, 230))
            cv2.imshow(win_name, output)

            if key_down != -1:
                if key_down == 27:
                    break
                char_key = chr(key_down)
                # playback
                if char_key == 'p':
                    playback_enabled = True
                    curr_processor.stop()
                    curr_idx = 0
                    curr_key = demo_key_list[curr_idx]
                    processor_dict[curr_key].start()
                    playback_start = timeit.default_timer()
                # manual switch
                elif char_key in processor_dict and char_key != curr_key:
                    playback_enabled = False
                    if char_key != curr_key:
                        curr_processor.stop()
                        curr_key = char_key
                        processor_dict[curr_key].start()
                # in-demo action
                elif char_key in curr_actions:
                    curr_action = curr_actions[char_key]
                    curr_processor.__getattribute__(curr_action['action'])(*curr_action['args'])

            if playback_enabled and timeit.default_timer() - playback_start > playback_duration:
                curr_idx = (curr_idx + 1) % len(demo_key_list)
                curr_key = demo_key_list[curr_idx]
                playback_start = timeit.default_timer()
    finally:
        if power_meter is not None:
            power_meter.release()
        emulator.release()
        cv2.destroyAllWindows()
        for processor in processor_dict.values():
            processor.release()


if __name__ == '__main__':
    main()
