import cv2
import numpy as np
import math
from typing import Tuple
from threading import Thread
import spu_emulator.smart_event as smart_event


class SpuEmulator:
    def __init__(self, resolution: Tuple[int, int],
                 use_picam=True, picam_config=None,
                 motion_threshold=25,
                 profile_chnl=16,
                 profile_divisor=64):
        self.stopped = True
        self.thread = None
        self.resolution = resolution
        self.motion_threshold = motion_threshold
        self.profile_chnl = profile_chnl
        self.profile_divisor = profile_divisor

        w, h = resolution
        w_profile, h_profile = math.ceil(w / profile_divisor), math.ceil(h / profile_divisor)
        self.frame = np.zeros((h, w, 3), np.uint8)
        self.prev_gray = np.zeros((h, w), np.uint8)
        self.event_frame = np.zeros((h, w), np.uint8)
        self.grid_col_profiles = np.zeros((profile_chnl, h_profile, w_profile * profile_divisor), int)
        self.grid_row_profiles = np.zeros((profile_chnl, h_profile * profile_divisor, w_profile), int)

        self.use_picam = use_picam
        if self.use_picam:
            from picamera2 import Picamera2
            from libcamera import controls
            self.picam2 = Picamera2()
            self.picam2.preview_configuration.main.size = self.resolution
            self.picam2.preview_configuration.main.format = "RGB888"
            self.picam2.configure("preview")
            # Get Auto Focus Mode defined by libcamera
            if picam_config["af_mode"] is True:
                af_mode = controls.AfModeEnum.Continuous
            else:
                af_mode = controls.AfModeEnum.Manual

            # Check bounds of lens position
            lens_position = picam_config["lens_position"]
            if lens_position < 0.0:
                lens_position = 0.0
            elif lens_position > 10.0:
                lens_position = 10.0

            # Configure the camera
            self.picam2.set_controls(
                {
                    "AeEnable": picam_config["ae_enable"],  # AEC/AGC algorithm
                    "AfMode": af_mode,  # Manual focus or Continuous auto focus
                    "LensPosition": lens_position,
                    "NoiseReductionMode": controls.draft.NoiseReductionModeEnum.Off,
                    "AwbEnable": picam_config["awb_enable"],  # Automatic white balance
                    "ColourGains": (2.0, 2.0),  # To compensate the 2 Green pixels in Bayer pattern
                    "AnalogueGain": picam_config["analog_gain"],
                    "ExposureTime": picam_config["exposure_us"],  # Exposure time [microseconds]
                    "FrameRate": 60
                }
            )
        else:
            self.cap = cv2.VideoCapture(0)
            self.cap.set(cv2.CAP_PROP_AUTO_WB, 0.0)  # Disable automatic white balance
            self.cap.set(cv2.CAP_PROP_WB_TEMPERATURE, 4200)  # Set manual white balance temperature to 4200K

    def start(self):
        if not self.stopped:
            return
        self.stopped = False
        if self.use_picam:
            self.picam2.start()
        self.thread = Thread(target=self.update, args=())
        self.thread.start()

    def stop(self):
        self.stopped = True
        if self.thread is not None:
            self.thread.join()

    def release(self):
        self.stop()
        if self.use_picam:
            self.picam2.close()
        else:
            self.cap.release()

    def update(self):
        while True:
            if self.stopped:
                return
            if self.use_picam:
                frame = self.picam2.capture_array()
            else:
                _, frame = self.cap.read()
            if frame is None:
                continue
            if frame.shape[0] != self.resolution[1] or frame.shape[1] != self.resolution[0]:
                frame = cv2.resize(frame, self.resolution)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            event_frame = smart_event.motion_event(gray, self.prev_gray, self.motion_threshold)
            grid_col_profile, grid_row_profile = smart_event.grid_profile(event_frame, self.profile_divisor)
            self.frame = frame
            self.prev_gray = gray
            self.event_frame = event_frame
            self.grid_col_profiles[:-1], self.grid_col_profiles[-1] = self.grid_col_profiles[1:], grid_col_profile
            self.grid_row_profiles[:-1], self.grid_row_profiles[-1] = self.grid_row_profiles[1:], grid_row_profile

    def read_frame(self):
        return self.frame.copy()

    def read_motion_mask(self):
        return (self.event_frame > 0).astype(np.uint8)

    def read_motion_frame(self):
        return self.event_frame.copy()

    def read_color_mask(self, color_code=cv2.COLOR_BGR2YCrCb, threshold_idx=0):
        return smart_event.color_mask(self.frame, color_code, threshold_idx)

    def read_color_frame(self, color_code=cv2.COLOR_BGR2YCrCb, threshold_idx=0):
        return smart_event.color_event(self.frame, color_code, threshold_idx)

    def read_grid_profiles(self):
        return self.grid_col_profiles.copy(), self.grid_row_profiles.copy()

    def read_col_profile(self):
        return smart_event.compute_col_profile(self.event_frame)

    def read_row_profile(self):
        return smart_event.compute_row_profile(self.event_frame)

    def read_profile(self):
        return smart_event.compute_profile(self.event_frame)
