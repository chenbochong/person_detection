####################################################################
# foreground_extraction_events.py
#
# Copyright: (C) 2019-2022 FRIS Inc. (dba Oculi)
####################################################################

import cv2
import logging
import numpy as np
import timeit


class ForegroundExtractionEvents():
    def __init__(self, input_size):
        self.cols = input_size[0]  # input_size = (width, height)
        self.rows = input_size[1]

        self.gray_frame = np.zeros((self.rows, self.cols), dtype=np.uint8)
        self.gray_background = np.zeros((self.rows, self.cols), dtype=np.uint8)
        self.first_iteration = True

        self.diff = np.zeros((self.rows, self.cols), dtype=np.uint8)
        self.mask = np.zeros((self.rows, self.cols), dtype=np.uint8)

        # Use a single threshold and absolute difference to determine
        # foreground_extraction. This simplifies the logic for this module at the expense
        # of making the code generic.
        self.threshold = 0
        self.reset_thresholds()

        self.bandwidth = 0.0

    def reset_thresholds(self):
        '''
        Initialize foreground_extraction thresholds.
        '''
        self.threshold = 25

    def set_thresholds(self, value):
        '''
        Update foreground_extraction thresholds.
        '''
        self.threshold = value

    # def find_mask(self, frame):
    #     '''
    #     Find mask based on grayscale delta thresholds. This is to simulate
    #     the foreground_extraction smart events. Returns a mask of the activated pixels.
    #     '''
    #     # Convert to grayscale for differencing (ping pong between two
    #     # buffers in order to avoid a deep copy)
    #     start = timeit.default_timer()
    #     if self.gray2_is_prev is True:
    #         cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY, dst=self.gray1)
    #     else:
    #         cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY, dst=self.gray2)
    #     logging.debug(f'cvtColor: {1e3*(timeit.default_timer() - start):1.2f} ms')

    #     # Find (abs) difference between current and previous frame. Only
    #     # look for large changes in foreground_extraction for this module.
    #     start = timeit.default_timer()
    #     self.diff = cv2.absdiff(self.gray1, self.gray2)
    #     logging.debug(f'absdiff: {1e3*(timeit.default_timer() - start):1.2f} ms')

    #     # Update ping pong buffer flag
    #     self.gray2_is_prev = not(self.gray2_is_prev)

    #     # Find pixel locations that would have triggered event (based on large changes)
    #     start = timeit.default_timer()
    #     np.greater_equal(self.diff, self.threshold, out=self.mask)
    #     logging.debug(f'greater_equal: {1e3*(timeit.default_timer() - start):1.2f} ms')

    def find_mask(self, frame):
        '''
        Find mask based on grayscale delta thresholds. This is to simulate
        the foreground_extraction smart events. Returns a mask of the activated pixels.
        '''
        # Convert to grayscale for differencing (ping pong between two
        # buffers in order to avoid a deep copy)
        start = timeit.default_timer()
        if self.first_iteration is True:
            cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY, dst=self.gray_frame)
            self.gray_background = self.gray_frame.copy()
            self.first_iteration = False
        else:
            cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY, dst=self.gray_frame)
        logging.debug(f'cvtColor: {1e3 * (timeit.default_timer() - start):1.2f} ms')

        # Find (abs) difference between current and previous frame. Only
        # look for large changes in foreground_extraction for this module.
        start = timeit.default_timer()
        self.diff = cv2.absdiff(self.gray_frame, self.gray_background)
        logging.debug(f'absdiff: {1e3 * (timeit.default_timer() - start):1.2f} ms')

        # Find pixel locations that would have triggered event (based on large changes)
        start = timeit.default_timer()
        np.greater_equal(self.diff, self.threshold, out=self.mask)
        logging.debug(f'greater_equal: {1e3 * (timeit.default_timer() - start):1.2f} ms')

    def set_bcgrnd_frame(self, frame):
        cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY, dst=self.gray_frame)
        self.gray_background = self.gray_frame.copy()

    def apply_mask(self, frame):
        mask_uint8 = self.mask.astype(np.uint8)

        return cv2.bitwise_and(frame, frame, mask=mask_uint8)

    def find_and_apply_mask(self, frame):
        '''
        Find mask based on thresholds in the specified color space. Then apply this mask to
        the input BGR frame and return that frame.
        '''
        self.find_mask(frame)
        return self.apply_mask(frame)
