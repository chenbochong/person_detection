####################################################################
# File : smart_event.py
#
# Copyright: (C) 2019-2024 FRIS Inc. (dba Oculi)
####################################################################

import cv2
import numpy as np
import math
import const


def motion_event(frame, prev_frame, threshold):
    mask = cv2.absdiff(frame, prev_frame) > threshold
    event_frame = np.zeros_like(frame, np.uint8)
    event_frame[mask] = frame[mask]
    return event_frame


def color_mask(frame, color_code=cv2.COLOR_BGR2YCrCb, threshold_idx=0):
    # set threshold
    if threshold_idx == 1:
        threshold_lo, threshold_hi = const.LOWER_YCrCb_1, const.UPPER_YCrCb_1
    else:
        threshold_lo, threshold_hi = const.LOWER_YCrCb_0, const.UPPER_YCrCb_0
    frame_new = cv2.cvtColor(frame, color_code)
    mask = cv2.inRange(frame_new, threshold_lo, threshold_hi)
    mask[mask > 0] = 1
    return mask


def color_event(frame, color_code=cv2.COLOR_BGR2YCrCb, threshold_idx=0):
    mask = color_mask(frame, color_code, threshold_idx)
    return cv2.bitwise_and(frame, frame, mask=mask)


def grid_profile(img, divisor):
    assign_h = math.ceil(img.shape[0] / divisor) * divisor
    assign_w = math.ceil(img.shape[1] / divisor) * divisor
    if assign_h != img.shape[0] or assign_w != img.shape[1]:
        img = np.pad(img, ((0, assign_h - img.shape[0]), (0, assign_w - img.shape[1])))
    col_profile = np.count_nonzero(img.reshape(-1, divisor, img.shape[1]), axis=1)
    row_profile = np.count_nonzero(img.reshape(img.shape[0], -1, divisor), axis=2)
    return col_profile, row_profile


def compute_col_profile(img):
    return np.count_nonzero(img, axis=0)


def compute_row_profile(img):
    return np.count_nonzero(img, axis=1)


def compute_profile(img):
    col_profile = compute_col_profile(img)
    row_profile = compute_row_profile(img)
    return col_profile, row_profile
