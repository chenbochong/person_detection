####################################################################
# region_finder.py
#
# Class to look at data (real or simulated) from the SPU and generate
# a region of interest based on the activation energy.
#
# Copyright: (C) 2019-2022 FRIS Inc. (dba Oculi)
####################################################################

import cv2
import logging
import numpy as np
from scipy.signal import find_peaks
import timeit

from spu_emulator import box


def check_overlap(roi_list, new_roi):
    '''
    Check if new ROI overlaps with list of ROI's.
    Return True if there is an overlap, else False.
    '''
    for roi in roi_list:
        # Check for no overlap between roi and new_roi
        if box.intersection_over_union(roi, new_roi) > 0:
            return True
    return False


def compute_col_profile(event_mask):
    return cv2.reduce(event_mask, dim=0, rtype=cv2.REDUCE_SUM, dtype=cv2.CV_32S).flatten()


def compute_row_profile(event_mask):
    return cv2.reduce(event_mask, dim=1, rtype=cv2.REDUCE_SUM, dtype=cv2.CV_32S).flatten()


class ProfileRegionFinder():
    def __init__(self, input_size):
        self.cols = input_size[0]  # input_size = (width, height)
        self.rows = input_size[1]

        self.col_profile = np.zeros(self.cols)
        self.row_profile = np.zeros(self.rows)

    def find_energy(self, profile):
        # Find the peaks in the activation compute_profile
        peaks, pk_props = find_peaks(profile, height=15, rel_height=1, width=20)

        # Iterate over the peaks and estimate the one with the most energy
        return self.max_energy(pk_props)

    def max_energy(self, pk_props):
        '''
        Iterate over the peaks and estimate the one with the most energy.
        '''
        max_energy = 0
        x = 0
        w = 0
        for i in range(len(pk_props['widths'])):
            energy = pk_props['widths'][i] * pk_props['peak_heights'][i]
            if energy > max_energy:
                max_energy = energy
                x = int(pk_props['left_ips'][i])
                w = int(pk_props['widths'][i])
        return x, w

    def compute_centroid(self, x):
        '''
        Compute the centroid of input array.
        '''
        indices = np.arange(0, x.shape[0]) + 1
        return np.sum(x * indices) // np.sum(x)

    def find_regions(self, events, fixed_size=(0, 0)):
        # Find the x coordinates / widths using the activation energy along
        # the columns activation compute_profile.
        start = timeit.default_timer()
        self.col_profile = compute_col_profile(events)
        logging.debug(f'compute_col_profile: {1e3 * (timeit.default_timer() - start):1.2f} ms')

        start = timeit.default_timer()
        peaks, pk_props = find_peaks(self.col_profile, height=15, rel_height=1, width=50, distance=100, prominence=15)
        logging.debug(f'find_peaks: {1e3 * (timeit.default_timer() - start):1.2f} ms')

        # Find the y coordinate and height using the activation energy along
        # the rows. This code assumes that we can compute that sum along a region
        # of interest and that we are not limited to all of the columns in a given row!
        regions = []
        fixed_h, fixed_w = fixed_size
        rows, cols = events.shape
        for i in range(len(peaks)):
            # Extract x and width of each peak
            x = int(pk_props['left_ips'][i])
            w = int(pk_props['widths'][i])

            # If no peak found (width of 0), then continue with next peak from column compute_profile.
            if w == 0 or w < 20:
                continue

            # Find horizontal compute_profile based only on the columns in each peak.
            start = timeit.default_timer()
            self.row_profile = compute_row_profile(events[:, x:x + w])
            logging.debug(f'compute_row_profile: {1e3 * (timeit.default_timer() - start):1.2f} ms')

            start = timeit.default_timer()
            y, h = self.find_energy(self.row_profile)
            logging.debug(f'find_energy: {1e3 * (timeit.default_timer() - start):1.2f} ms')

            # If no peak found (height of 0), then continue with next peak from column compute_profile.
            if h == 0 or h < 20:
                continue

            # Filter based on ratio of width to height?
            # ratio = w/h

            # Find bounding box/region based on either the centroid of the column/row peaks or
            # based on the absolute width of the column/row peaks.
            if fixed_size != (0, 0):
                # Find centroid of the chosen peaks
                start = timeit.default_timer()
                centroid_x = self.compute_centroid(self.col_profile[x:x + w]) + x
                centroid_y = self.compute_centroid(self.row_profile[y:y + h]) + y
                logging.debug(f'compute_centroid: {1e3 * (timeit.default_timer() - start):1.2f} ms')

                x = centroid_x - fixed_w // 2
                w = fixed_w
                y = centroid_y - fixed_h // 2
                h = fixed_h

                # Resize region to be within image bounds. Keeps fixed width/height.
                (x, y, w, h) = box.check_bounds2((x, y, w, h), rows, cols)
            else:
                # Resize region to be within image bounds. Dynamic width/height.
                (x, y, w, h) = box.check_bounds((x, y, w, h), rows, cols)

            # Make sure ROI doesn't overlap
            if len(regions) == 0:
                regions.append((x, y, w, h))
            elif not check_overlap(regions, (x, y, w, h)):
                regions.append((x, y, w, h))

        return regions
