import torch

from model import ProfileNet

weight_path = 'weights/profile_person_det.pth'
backbone = dict(
    stage_channels=[[16, 16, 16], [16, 64], [64, 64], [64, 64], [64, 64], [64, 64]],
    downsample_idx=[0, 2, 3, 4],
    out_idx=[3, 4, 5]
)
neck = dict(
    in_channels=[64, 64, 64],
    out_idx=[0, 1, 2]
)
bbox_head = dict(
    num_classes=2,
    in_channels=64,
    shared_stacked_convs=1,
    stacked_convs=0,
    feat_channels=64,
    strides=[8, 16, 32]
)

torch_model = ProfileNet(backbone, neck, bbox_head)
state_dict = torch.load(weight_path, map_location=torch.device('cpu'))['state_dict']
torch_model.load_state_dict(state_dict, strict=False)
col_profile = torch.randn(1, 16, 8, 640)
row_profile = torch.randn(1, 16, 512, 10)

torch.onnx.export(torch_model,
                  args=(col_profile, row_profile),
                  f='onnx/profile_net.onnx',
                  input_names=['col_profile', 'row_profile'],
                  output_names=['cls_8', 'cls_16', 'cls_32', 'bbox_8', 'bbox_16', 'bbox_32', 'obj_8', 'obj_16', 'obj_32'])

