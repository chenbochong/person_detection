import torch.nn as nn

from .layers.roi_net_backbone import RoiNetBackbone
from .layers.detection_neck import DetectionNeck
from .layers.detection_head import DetectionHead


class RoiNet(nn.Module):

    def __init__(self, backbone_config, neck_config, head_config):
        super().__init__()
        self.backbone = RoiNetBackbone(**backbone_config)
        self.neck = DetectionNeck(**neck_config)
        self.bbox_head = DetectionHead(**head_config)

    def forward(self, img):
        feats = self.backbone.forward(img)
        feats = self.neck.forward(feats)
        cls_preds, bbox_preds, obj_preds = self.bbox_head.forward(feats)
        return cls_preds, bbox_preds, obj_preds