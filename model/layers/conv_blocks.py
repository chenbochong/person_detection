import torch.nn as nn


class ConvDPUnit(nn.Module):
    def __init__(self, in_channels, out_channels, withBNRelu=True):
        super(ConvDPUnit, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = nn.Conv2d(
            in_channels, out_channels, kernel_size=(1, 1))
        self.conv2 = nn.Conv2d(
            out_channels, out_channels, kernel_size=(3, 3), padding=(1, 1), groups=out_channels)
        self.withBNRelu = withBNRelu
        if withBNRelu:
            self.bn = nn.BatchNorm2d(out_channels)
            self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        if self.withBNRelu:
            x = self.bn(x)
            x = self.relu(x)
        return x


class ConvDPUnit1D(nn.Module):
    def __init__(self, in_channels, out_channels, withBNRelu=True):
        super(ConvDPUnit1D, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = nn.Conv1d(
            in_channels, out_channels, kernel_size=(1,))
        self.conv2 = nn.Conv1d(
            out_channels, out_channels, kernel_size=(3,), padding=(1,), groups=out_channels)
        self.withBNRelu = withBNRelu
        if withBNRelu:
            self.bn = nn.BatchNorm1d(out_channels)
            self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        if self.withBNRelu:
            x = self.bn(x)
            x = self.relu(x)
        return x


class ConvHead(nn.Module):
    def __init__(self, in_channels, mid_channels, out_channels, stride=(2, 2)):
        super(ConvHead, self).__init__()
        self.in_channels = in_channels
        self.mid_channels = mid_channels
        self.out_channels = out_channels
        self.conv1 = nn.Conv2d(
            in_channels, mid_channels, kernel_size=(3, 3), stride=stride, padding=(1, 1))
        self.conv2 = ConvDPUnit(mid_channels, out_channels, True)
        self.bn1 = nn.BatchNorm2d(mid_channels)
        self.relu1 = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)
        x = self.conv2(x)
        return x


class ConvHead1D(nn.Module):
    def __init__(self, in_channels, mid_channels, out_channels):
        super(ConvHead1D, self).__init__()
        self.in_channels = in_channels
        self.mid_channels = mid_channels
        self.out_channels = out_channels
        self.conv1 = nn.Conv1d(
            in_channels, mid_channels, kernel_size=(3,), stride=(2,), padding=(1,))
        self.conv2 = ConvDPUnit1D(mid_channels, out_channels, True)
        self.bn1 = nn.BatchNorm1d(mid_channels)
        self.relu1 = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu1(x)
        x = self.conv2(x)
        return x


class Conv4layerBlock(nn.Module):
    def __init__(self, in_channels, out_channels, withBNRelu=True):
        super(Conv4layerBlock, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = ConvDPUnit(in_channels, in_channels, True)
        self.conv2 = ConvDPUnit(in_channels, out_channels, withBNRelu)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        return x


class Conv4layerBlock1D(nn.Module):
    def __init__(self, in_channels, out_channels, withBNRelu=True):
        super(Conv4layerBlock1D, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.conv1 = ConvDPUnit1D(in_channels, in_channels, True)
        self.conv2 = ConvDPUnit1D(in_channels, out_channels, withBNRelu)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        return x
