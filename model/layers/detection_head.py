import torch.nn as nn

from .conv_blocks import ConvDPUnit


class DetectionHead(nn.Module):
    def __init__(self, num_classes, in_channels, strides,
                 feat_channels=256, shared_stacked_convs=2, stacked_convs=2
                 ):

        super().__init__()
        self.num_classes = num_classes
        self.cls_out_channels = num_classes
        self.in_channels = in_channels
        self.feat_channels = feat_channels
        self.stacked_convs = stacked_convs
        self.use_sigmoid_cls = True
        self.shared_stack_convs = shared_stacked_convs
        self.strides = strides
        self.strides_num = len(self.strides)

        self._init_layers()

    def _init_layers(self):
        if self.shared_stack_convs > 0:
            self.multi_level_share_convs = nn.ModuleList()
        if self.stacked_convs > 0:
            self.multi_level_cls_convs = nn.ModuleList()
            self.multi_level_reg_convs = nn.ModuleList()
        self.multi_level_cls = nn.ModuleList()
        self.multi_level_bbox = nn.ModuleList()
        self.multi_level_obj = nn.ModuleList()
        for _ in self.strides:
            if self.shared_stack_convs > 0:
                single_level_share_convs = []
                for i in range(self.shared_stack_convs):
                    chn = self.in_channels if i == 0 else self.feat_channels
                    single_level_share_convs.append(
                        ConvDPUnit(chn, self.feat_channels))
                self.multi_level_share_convs.append(
                    nn.Sequential(*single_level_share_convs))

            if self.stacked_convs > 0:
                single_level_cls_convs = []
                single_level_reg_convs = []
                for i in range(self.stacked_convs):
                    chn = self.in_channels if i == 0 and \
                                              self.shared_stack_convs == 0 else self.feat_channels
                    single_level_cls_convs.append(
                        ConvDPUnit(chn, self.feat_channels))
                    single_level_reg_convs.append(
                        ConvDPUnit(chn, self.feat_channels))
                self.multi_level_reg_convs.append(
                    nn.Sequential(*single_level_reg_convs))
                self.multi_level_cls_convs.append(
                    nn.Sequential(*single_level_cls_convs))

            chn = self.in_channels if self.stacked_convs == 0 and \
                                      self.shared_stack_convs == 0 else self.feat_channels
            self.multi_level_cls.append(
                ConvDPUnit(chn, self.num_classes, False))
            self.multi_level_bbox.append(ConvDPUnit(chn, 4, False))
            self.multi_level_obj.append(ConvDPUnit(chn, 1, False))

    def forward(self, feats):
        feats = [
            convs(feat)
            for feat, convs in zip(feats, self.multi_level_share_convs)
        ]

        cls_preds = [
            convs(feat) for feat, convs in zip(feats, self.multi_level_cls)
        ]
        bbox_preds = [
            convs(feat)
            for feat, convs in zip(feats, self.multi_level_bbox)
        ]
        obj_preds = [
            convs(feat) for feat, convs in zip(feats, self.multi_level_obj)
        ]

        return cls_preds, bbox_preds, obj_preds
