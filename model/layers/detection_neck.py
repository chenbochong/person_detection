import torch.nn as nn
import torch.nn.functional as F

from .conv_blocks import ConvDPUnit


class DetectionNeck(nn.Module):
    def __init__(self, in_channels, out_idx, scale_factors=None):
        super().__init__()
        self.num_layers = len(in_channels)
        self.out_idx = out_idx
        self.scale_factors = [2.] * len(out_idx) if scale_factors is None else scale_factors
        self.lateral_convs = nn.ModuleList()
        for i in range(self.num_layers):
            self.lateral_convs.append(
                ConvDPUnit(in_channels[i], in_channels[i], True))

    def forward(self, feats):
        num_feats = len(feats)

        # top-down flow
        for i in range(num_feats - 1, 0, -1):
            feats[i] = self.lateral_convs[i](feats[i])
            feats[i - 1] = feats[i - 1] + F.interpolate(
                feats[i], scale_factor=self.scale_factors[i], mode='nearest')

        feats[0] = self.lateral_convs[0](feats[0])

        outs = [feats[i] for i in self.out_idx]
        return outs