import torch
import torch.nn as nn
import torch.nn.functional as F

from .conv_blocks import ConvHead, Conv4layerBlock, ConvDPUnit


class ProfileBackbone(nn.Module):
    def __init__(self, stage_channels, downsample_idx, out_idx):
        super().__init__()
        self.layer_num = len(stage_channels)
        self.downsample_idx = downsample_idx
        self.out_idx = out_idx
        self.col_model0 = ConvHead(*stage_channels[0], stride=(1, 2))
        self.row_model0 = ConvHead(*stage_channels[0], stride=(2, 1))
        for i in range(1, self.layer_num):
            self.add_module(f'col_model{i}', Conv4layerBlock(*stage_channels[i]))
            self.add_module(f'row_model{i}', Conv4layerBlock(*stage_channels[i]))
        for idx in self.out_idx:
            out_channel = stage_channels[idx][-1]
            self.add_module(f'out_model{idx}', ConvDPUnit(out_channel * 2, out_channel))

    def forward(self, col_profile, row_profile):
        out = []
        for i in range(self.layer_num):
            col_profile = self.__getattr__(f'col_model{i}')(col_profile)
            row_profile = self.__getattr__(f'row_model{i}')(row_profile)
            if i in self.out_idx:
                col_x = torch.repeat_interleave(col_profile, int(row_profile.shape[2] / col_profile.shape[2]), dim=2)
                row_x = torch.repeat_interleave(row_profile, int(col_profile.shape[3] / row_profile.shape[3]), dim=3)
                x = torch.cat((col_x, row_x), 1)
                x = self.__getattr__(f'out_model{i}')(x)
                out.append(x)
            if i in self.downsample_idx:
                col_profile = F.max_pool2d(col_profile, (1, 2))
                row_profile = F.max_pool2d(row_profile, (2, 1))
        return out