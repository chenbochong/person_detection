import torch.nn as nn
import torch.nn.functional as F

from .conv_blocks import ConvHead, Conv4layerBlock


class RoiNetBackbone(nn.Module):
    def __init__(self, stage_channels, downsample_idx, out_idx):
        super().__init__()
        self.layer_num = len(stage_channels)
        self.downsample_idx = downsample_idx
        self.out_idx = out_idx
        self.model0 = ConvHead(*stage_channels[0])
        for i in range(1, self.layer_num):
            self.add_module(f'model{i}', Conv4layerBlock(*stage_channels[i]))

    def forward(self, x):
        out = []
        for i in range(self.layer_num):
            x = self.__getattr__(f'model{i}')(x)
            if i in self.out_idx:
                out.append(x)
            if i in self.downsample_idx:
                x = F.max_pool2d(x, 2)
        return out
