from .base_demo import BaseDemo


class FullFrameDemo(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)

    def compute_output_frame(self):
        return self.emulator.read_frame()
