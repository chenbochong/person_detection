import cv2
from .base_demo import BaseDemo


class MotionEventDemo(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)

    def compute_output_frame(self):
        event_frame = self.emulator.read_motion_frame()
        return cv2.cvtColor(event_frame, cv2.COLOR_GRAY2BGR)
