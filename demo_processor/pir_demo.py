# from .base_demo import BaseDemo
#
#
# class PIRDemo(BaseDemo):
#     def __init__(self, emulator, power_meter):
#         super().__init__(emulator, power_meter)
#
#     def compute_output_frame(self):
#         output = self.emulator.read_frame()
#         return output


import gpiod
import cv2
import numpy as np
import time
from threading import Thread

from .base_demo import BaseDemo


class PIRDemo(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)
        self.stopped = True
        self.thread = None
        self.input_pin = 18
        chip = gpiod.Chip('gpiochip4')
        self.in_line = chip.get_line(self.input_pin)
        self.in_line.request(consumer="PIR", type=gpiod.LINE_REQ_DIR_IN)
        self.input_state = 0

    def start(self):
        if not self.stopped:
            return
        self.stopped = False
        self.thread = Thread(target=self.update, args=())
        self.thread.start()

    def stop(self):
        self.stopped = True
        if self.thread is not None:
            self.thread.join()

    def release(self):
        self.stop()
        self.in_line.release()

    def update(self):
        while True:
            if self.stopped:
                return
            self.input_state = self.in_line.get_value()
            time.sleep(0.1)

    def compute_output_frame(self):
        output = np.zeros((self.resolution[1], self.resolution[0], 3), dtype=np.uint8)
        if self.input_state == 1:
            output = cv2.putText(output, 'Presence', (100, 200), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 255, 0), 3,
                                 cv2.LINE_AA)
            output = cv2.putText(output, 'Detected', (100, 300), cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 255, 0), 3,
                                 cv2.LINE_AA)
        return output
