import numpy as np


class BaseDemo:
    def __init__(self, emulator, power_meter, *args, **kwargs):
        self.resolution = emulator.resolution
        self.emulator = emulator
        self.power_meter = power_meter

    def compute_output_frame(self):
        return np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)

    def start(self):
        return

    def stop(self):
        return

    def release(self):
        return
