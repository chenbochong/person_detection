import cv2
from .base_demo import BaseDemo


class ColorEventDemo(BaseDemo):
    def __init__(self, emulator, power_meter,
                 color_code=cv2.COLOR_BGR2YCrCb, threshold_idx=0):
        super().__init__(emulator, power_meter)
        self.threshold_idx = threshold_idx
        self.color_code = color_code

    def set_threshold(self, threshold_idx):
        self.threshold_idx = threshold_idx

    def compute_output_frame(self):
        return self.emulator.read_color_frame(self.color_code, self.threshold_idx)
