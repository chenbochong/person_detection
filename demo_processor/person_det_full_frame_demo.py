import cv2
import const
from .base_demo import BaseDemo
from detector import RoiPersonDetector


class PersonDetFullFrameDemo(BaseDemo):
    def __init__(self, emulator, power_meter, weight_path, conf_threshold=0.9):
        super().__init__(emulator, power_meter)
        self.detector = RoiPersonDetector(weight_path, class_names=['Person'], conf_threshold=conf_threshold)

    def compute_output_frame(self):
        frame = self.emulator.read_frame()
        results = self.detector.detect(frame)

        output = frame
        for result in results:
            # Only display person
            if result['cls'] != 0:
                continue
            x, y, w, h = result['bbox']
            score = result['score']
            cls_name = result['cls_name']
            cv2.rectangle(output, (x, y), (x + w, y + h), const.COLOR_PRED, 1)
            cv2.putText(output, f'{cls_name}: {score:.2f}', (x, y - 10),
                        const.FONT, const.FONT_SCALE, const.COLOR_PRED, const.FONT_THICKNESS)
        return output
