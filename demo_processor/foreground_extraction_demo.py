import cv2
import numpy as np
from scipy.signal import savgol_filter, argrelmax
from .base_demo import BaseDemo
from spu_emulator import foreground_extraction_events


def get_actionable_signals(gray_frame):
    """
    Extracts actionable signals from a grayscale image.

    Parameters:
        gray_frame (numpy.ndarray): Grayscale input image.

    Returns:
        tuple: A tuple containing two numpy arrays:
            - number_of_triggered_pixels_per_col: An array containing the count of non-black pixels for each column.
            - number_of_triggered_pixels_per_row: An array containing the count of non-black pixels for each row.
    """

    # Threshold the grayscale image to get binary image
    _, binary = cv2.threshold(gray_frame, 1, 255, cv2.THRESH_BINARY)

    # Count non-black pixels for each column
    number_of_triggered_pixels_per_col = np.sum(binary > 0, axis=0)

    # Count non-black pixels for each row
    number_of_triggered_pixels_per_row = np.sum(binary > 0, axis=1)

    return number_of_triggered_pixels_per_col, number_of_triggered_pixels_per_row


def get_rectangle_borders(maxima_indexes, distance_center_border):
    """
    Calculates the borders of rectangles around row maxima.

    Parameters:
        maxima_indexes (list): A list of indexes representing maxima.
        distance_center_border (int): The distance from the center to the border of the rectangle.

    Returns:
        list: A list of tuples, where each tuple contains two integers representing the lower and upper borders
              of a rectangle around a maximum.
    """

    rectangle_borders = []  # Initialize an empty list to store the lower and upper borders of rectangles

    for max_index in maxima_indexes:
        # Calculate the lower and upper borders of the rectangle around the row maximum
        lower_border = max_index - distance_center_border
        upper_border = max_index + distance_center_border
        rectangle_borders.append([lower_border, upper_border])  # Add the borders to the list

    return rectangle_borders  # Return the list of rectangle borders


def find_people_rectangles(row_sums, col_sums, maximums_min_threshold_value=20,
                           distance_from_rectangle_center_to_border=50):
    """
    Finds rectangles around people based on row and column sums.

    Parameters:
        row_sums (numpy.ndarray): Array containing the sums of pixel values along rows.
        col_sums (numpy.ndarray): Array containing the sums of pixel values along columns.
        maximums_min_threshold_value (int, optional): Minimum threshold value for maximums in row and column sums.
                                                      Defaults to 20.
        distance_from_rectangle_center_to_border (int, optional): Distance from the center to the border of the rectangle.
                                                                 Defaults to 30.

    Returns:
        list: A list of tuples, where each tuple represents the coordinates of a rectangle
              around a detected person.
    """
    # Smooth the row and column sums further to reduce noise
    smoothed_row_sums = savgol_filter(row_sums, 21, 3)
    smoothed_col_sums = savgol_filter(col_sums, 21, 3)

    # Find all the maximums in the smoothed signals
    row_maxima_indexes = argrelmax(smoothed_row_sums, order=1)[0]
    col_maxima_indexes = argrelmax(smoothed_col_sums, order=1)[0]

    # Threshold the maximums to only keep the ones high enough
    row_maxima_indexes = row_maxima_indexes[smoothed_row_sums[row_maxima_indexes] > maximums_min_threshold_value]
    col_maxima_indexes = col_maxima_indexes[smoothed_col_sums[col_maxima_indexes] > maximums_min_threshold_value]

    # Group the maximums that are close to one another into clusters to reduce the number of rectangles
    col_maxima_indexes = group_numbers(col_maxima_indexes, 10)
    row_maxima_indexes = group_numbers(row_maxima_indexes, 10)

    # Calculate the borders of rectangles around row and column maximums
    row_borders = get_rectangle_borders(row_maxima_indexes, distance_from_rectangle_center_to_border)
    col_borders = get_rectangle_borders(col_maxima_indexes, distance_from_rectangle_center_to_border)

    # Combine the borders from rows and columns to get the borders for people
    border_rectangles = []
    for row_min in row_borders:
        for col_min in col_borders:
            min_row = row_min[0]
            max_row = row_min[1]
            min_col = col_min[0]
            max_col = col_min[1]
            border_rectangles.append(((min_col, min_row), (max_col, max_row)))

    return border_rectangles


def count_non_black_pixels_in_rectangle(frame, coordinates):
    """
    Count the number of non-black pixels in the given rectangle within the frame.
    """
    min_col, min_row = coordinates[0]
    max_col, max_row = coordinates[1]

    cropped_frame = frame[min_row:max_row, min_col:max_col]
    non_black_pixels = cv2.countNonZero(cropped_frame)

    return non_black_pixels


def clean_coordinates(frame_gray, coordinates_list, threshold=50):
    """
    Clean the list of coordinates based on the number of non-black pixels.
    """
    cleaned_coordinates = []
    for coordinates in coordinates_list:
        non_black_count = count_non_black_pixels_in_rectangle(frame_gray, coordinates)
        if non_black_count >= threshold:
            cleaned_coordinates.append(coordinates)
    return cleaned_coordinates


def group_numbers(input_list, max_difference):
    """
    Groups numbers in the input list that are close to each other.

    Parameters:
        input_list (list): List of numbers to group.
        max_difference (int): Maximum difference allowed between consecutive numbers to be grouped.

    Returns:
        list: A list containing the centers of the grouped numbers.
    """
    if len(input_list) == 0:
        return []

    # Initialize variables
    grouped_numbers = []
    current_group = [input_list[0]]

    # Iterate over the input list starting from the second element
    for num in input_list[1:]:
        if num - current_group[-1] <= max_difference:
            # If the current number is within the max difference, add it to the current group
            current_group.append(num)
        else:
            # If the current number is outside the max difference, compute the center of the current group
            group_center = (current_group[0] + current_group[-1]) // 2
            grouped_numbers.append(group_center)
            # Start a new group with the current number
            current_group = [num]

    # Compute the center of the last group
    if current_group:
        group_center = (current_group[0] + current_group[-1]) // 2
        grouped_numbers.append(group_center)

    return grouped_numbers


def filter_out_rectangles_with_bad_dimensions(rectangles, rotate90, minimum_height, ratio=1.5):
    """
    Filters out rectangles with bad dimensions based on specified criteria.

    Parameters:
        rectangles (list): List of rectangles, each represented as a tuple of two tuples ((min_col, min_row), (max_col, max_row)).
        rotate90 (bool): Flag indicating whether the rectangles are rotated by 90 degrees.
        minimum_height (int): Minimum height of the rectangles.
        ratio (float, optional): Minimum aspect ratio of the rectangles. Defaults to 1.5.

    Returns:
        list: List of filtered rectangles satisfying the specified criteria.
    """
    rectangles_output_list = list()  # Initialize an empty list to store the filtered rectangles

    for rectangle in rectangles:
        min_col = rectangle[0][0]
        min_row = rectangle[0][1]
        max_col = rectangle[1][0]
        max_row = rectangle[1][1]

        if not rotate90:  # If rectangles are not rotated by 90 degrees
            if (max_row - min_row) > minimum_height:  # Check if the height is greater than the minimum height
                if ((max_row - min_row) / (
                        max_col - min_col)) >= ratio:  # Check if aspect ratio is greater than or equal to the specified ratio
                    rectangles_output_list.append(rectangle)  # Add the rectangle to the output list
        else:  # If rectangles are rotated by 90 degrees
            if (max_col - min_col) > minimum_height:  # Check if the width is greater than the minimum height
                if ((max_col - min_col) / (
                        max_row - min_row)) >= ratio:  # Check if aspect ratio is greater than or equal to the specified ratio
                    rectangles_output_list.append(rectangle)  # Add the rectangle to the output list

    return rectangles_output_list  # Return the list of filtered rectangles


def merge(rect1, rect2):
    """
    Merges two rectangles into a single rectangle.

    Parameters:
        rect1 (tuple): First rectangle represented as ((min_col, min_row), (max_col, max_row)).
        rect2 (tuple): Second rectangle represented as ((min_col, min_row), (max_col, max_row)).

    Returns:
        tuple: Merged rectangle represented as ((min_col, min_row), (max_col, max_row)).
    """
    # Extract coordinates of the rectangles
    min_col = min(rect1[0][0], rect2[0][0])  # Minimum column coordinate
    min_row = min(rect1[0][1], rect2[0][1])  # Minimum row coordinate
    max_col = max(rect1[1][0], rect2[1][0])  # Maximum column coordinate
    max_row = max(rect1[1][1], rect2[1][1])  # Maximum row coordinate

    # Return the merged rectangle
    return [(min_col, min_row), (max_col, max_row)]


def merge_rectangles(rectangles, threshold=2):
    """
    Merges overlapping rectangles in a list of rectangles.

    Parameters:
        rectangles (list): List of rectangles, each represented as a tuple of two tuples ((min_col, min_row), (max_col, max_row)).
        threshold (int, optional): Threshold value to determine if rectangles are close in rows and columns. Defaults to 2.

    Returns:
        list: List of merged rectangles.
    """
    merged_rectangles = rectangles.copy()  # Make a copy of the input list to avoid modifying it directly

    while True:
        merged = False  # Flag to track if any merging occurred in the current iteration
        for i in range(len(merged_rectangles)):
            for j in range(i + 1, len(merged_rectangles)):
                rect1 = merged_rectangles[i]
                rect2 = merged_rectangles[j]

                # Extract coordinates of rectangles
                min_col1, min_row1 = rect1[0]
                max_col1, max_row1 = rect1[1]
                min_col2, min_row2 = rect2[0]
                max_col2, max_row2 = rect2[1]

                # Check if the rectangles are close in rows and columns
                close_in_rows = min_row1 <= max_row2 + threshold and max_row1 >= min_row2 - threshold
                close_in_cols = min_col1 <= max_col2 + threshold and max_col1 >= min_col2 - threshold

                if close_in_rows and close_in_cols:
                    # Merge the rectangles if they overlap
                    merged_rect = merge(rect1, rect2)
                    merged_rectangles.remove(rect1)
                    merged_rectangles.remove(rect2)
                    merged_rectangles.append(merged_rect)
                    merged = True  # Set the flag to indicate merging occurred
                    break
            if merged:
                break

        if not merged:
            break  # If no merging occurred in the current iteration, exit the loop

    return merged_rectangles  # Return the list of merged rectangles


def filter_out_and_draw_rectangles(image, rectangles, minimum_height, merge_distance=5, minimum_ratio=1.2):
    """
    Draws filtered rectangles on an image.

    Parameters:
        image (numpy.ndarray): Input image on which rectangles will be drawn.
        rectangles (list): List of rectangles to be drawn, each represented as a tuple of two tuples ((min_col, min_row), (max_col, max_row)).
        minimum_height (int): Minimum height of the rectangles.
        merge_distance (int, optional): Distance threshold for merging overlapping rectangles. Defaults to 5.
        minimum_ratio (float, optional): Minimum aspect ratio of the rectangles. Defaults to 1.2.

    Returns:
        numpy.ndarray: Image with filtered rectangles drawn.
    """
    # Initialize list to store merged rectangles
    merged_rectangles = []

    # Sort rectangles by their top-left corner coordinates
    rectangles.sort(key=lambda x: (x[0][1], x[0][0]))

    # Merge overlapping rectangles
    merged_rectangles = merge_rectangles(rectangles, merge_distance)

    # Filter out rectangles with bad dimensions
    if len(merged_rectangles) > 0:
        merged_rectangles = filter_out_rectangles_with_bad_dimensions(merged_rectangles, False, minimum_height,
                                                                      minimum_ratio)

    # Draw rectangles around merged positions
    for rect in merged_rectangles:
        # Draw the rectangle
        cv2.rectangle(image, rect[0], rect[1], (0, 255, 0), 1)

    return image


class ForegroundExtractionDemo(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)
        self.fe_mask = foreground_extraction_events.ForegroundExtractionEvents(self.resolution)

    def compute_output_frame(self):
        output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
        frame = self.emulator.read_frame()
        # Get the foreground extraction smart events
        self.fe_mask.find_mask(frame)
        foreground_extraction_frame_bgr = self.fe_mask.apply_mask(frame)

        # Get the 1 dimentional event frame (grayscale)
        foreground_extraction_frame_gray = cv2.cvtColor(foreground_extraction_frame_bgr, cv2.COLOR_BGR2GRAY)

        # Get the actionable signals: number of triggered pixels per column and per row for the motion output
        fe_nbr_of_trig_pix_per_col, fe_nbr_of_trig_pix_per_row = get_actionable_signals(foreground_extraction_frame_gray)

        # Find and draw rectangles around detected people
        fe_rectangles_positions = find_people_rectangles(fe_nbr_of_trig_pix_per_row, fe_nbr_of_trig_pix_per_col)

        # Remove rectangles with not enough pixels inside them
        fe_rectangles_positions = clean_coordinates(foreground_extraction_frame_gray, fe_rectangles_positions, threshold=50)

        # Merge the small rectangles together and draw the acceptable ones
        filter_out_and_draw_rectangles(output, fe_rectangles_positions, int(self.resolution[1] / 4),
                                       merge_distance=20, minimum_ratio=0.5)
        return output

    def reset_background(self):
        frame = self.emulator.read_frame()
        self.fe_mask.set_bcgrnd_frame(frame)
