import cv2
import numpy as np
import random
from .base_demo import BaseDemo


class RandomAccessDemo(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)
        self.mode = 'rows_x_cols'
        self.percentage = 5

    def switch_display_mode(self, mode):
        self.mode = mode

    def compute_output_frame(self):
        frame = self.emulator.read_frame()
        if self.mode == 'rows_x_cols':
            output = self.random_pixel_output(frame)
        elif self.mode == 'cols':
            output = self.random_pixel_output_cols(frame)
        else:
            raise Exception(f'Random Access: display mode {self.mode} is unknown.')
        return output

    def random_pixel_output(self, input_image):
        # Get image dimensions
        height, width, channels = input_image.shape

        # Calculate number of pixels to output
        total_pixels = height * width
        pixels_to_output = int((self.percentage / 100) * total_pixels)

        # Create a blank black image to start with
        output_image = np.zeros((height, width, channels), dtype=np.uint8)

        # Randomly select pixels to output
        sampled_pixels = random.sample(range(total_pixels), pixels_to_output)

        # Copy selected pixels from input to output
        for pixel in sampled_pixels:
            y = pixel // width
            x = pixel % width
            output_image[y, x] = input_image[y, x]

        # Create a completely black background
        black_background = np.zeros((height, width, channels), dtype=np.uint8)

        # Composite the output image over the black background
        final_output = cv2.addWeighted(output_image, 1, black_background, 1, 0)

        return final_output

    def random_pixel_output_cols(self, input_image):
        # Get image dimensions
        height, width, channels = input_image.shape

        # Calculate number of rows and columns to output
        total_rows = height
        total_cols = width
        rows_to_output = int((self.percentage / 100) * total_rows)
        cols_to_output = int((self.percentage / 100) * total_cols)

        # Create a blank black image to start with
        output_image = np.zeros((height, width, channels), dtype=np.uint8)

        # Randomly select rows and columns to output
        sampled_rows = random.sample(range(total_rows), rows_to_output)
        sampled_cols = random.sample(range(total_cols), cols_to_output)

        # Copy selected rows from input to output
        for row in sampled_rows:
            output_image[row, :] = input_image[row, :]

        # Copy selected columns from input to output
        for col in sampled_cols:
            output_image[:, col] = input_image[:, col]

        # Create a completely black background
        black_background = np.zeros((height, width, channels), dtype=np.uint8)

        # Composite the output image over the black background
        final_output = cv2.addWeighted(output_image, 1, black_background, 1, 0)

        return final_output
