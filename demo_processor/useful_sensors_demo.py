# from .base_demo import BaseDemo
#
#
# class UsefulSensorsDemo(BaseDemo):
#     def __init__(self, emulator, power_meter):
#         super().__init__(emulator, power_meter)
#
#     def compute_output_frame(self):
#         output = self.emulator.read_frame()
#         return output


from .base_demo import BaseDemo
import const

import io
import fcntl
import struct
import cv2
import numpy as np
from threading import Thread
import time

# The person sensor has the I2C ID of hex 62, or decimal 98.
PERSON_SENSOR_I2C_ADDRESS = 0x62

# We will be reading raw bytes over I2C, and we'll need to decode them into
# data structures. These strings define the format used for the decoding, and
# are derived from the layouts defined in the developer guide.
PERSON_SENSOR_I2C_HEADER_FORMAT = "BBH"
PERSON_SENSOR_I2C_HEADER_BYTE_COUNT = struct.calcsize(
    PERSON_SENSOR_I2C_HEADER_FORMAT)

PERSON_SENSOR_FACE_FORMAT = "BBBBBBbB"
PERSON_SENSOR_FACE_BYTE_COUNT = struct.calcsize(PERSON_SENSOR_FACE_FORMAT)

PERSON_SENSOR_FACE_MAX = 4
PERSON_SENSOR_RESULT_FORMAT = PERSON_SENSOR_I2C_HEADER_FORMAT + \
                              "B" + PERSON_SENSOR_FACE_FORMAT * PERSON_SENSOR_FACE_MAX + "H"
PERSON_SENSOR_RESULT_BYTE_COUNT = struct.calcsize(PERSON_SENSOR_RESULT_FORMAT)

# I2C channel 1 is connected to the GPIO pins
I2C_CHANNEL = 1
I2C_PERIPHERAL = 0x703

# How long to pause between sensor polls.
PERSON_SENSOR_DELAY = 0.2


class UsefulSensorsDemo(BaseDemo):
    def __init__(self, emulator, power_meter, conf_threshold=0.8):
        super().__init__(emulator, power_meter)
        self.stopped = True
        self.thread = None
        self.bboxes = []
        self.conf_threshold = conf_threshold
        self.i2c_handle = io.open("/dev/i2c-" + str(I2C_CHANNEL), "rb+", buffering=0)
        fcntl.ioctl(self.i2c_handle, I2C_PERIPHERAL, PERSON_SENSOR_I2C_ADDRESS)

    def start(self):
        if not self.stopped:
            return
        self.stopped = False
        self.thread = Thread(target=self.update, args=())
        self.thread.start()

    def stop(self):
        self.stopped = True
        if self.thread is not None:
            self.thread.join()

    def release(self):
        self.stop()
        self.i2c_handle.close()

    def update(self):
        while True:
            if self.stopped:
                return
            try:
                read_bytes = self.i2c_handle.read(PERSON_SENSOR_RESULT_BYTE_COUNT)
            except OSError as error:
                print("No person sensor data found")
                print(error)
                time.sleep(PERSON_SENSOR_DELAY)
                continue
            offset = 0
            (pad1, pad2, payload_bytes) = struct.unpack_from(
                PERSON_SENSOR_I2C_HEADER_FORMAT, read_bytes, offset)
            offset = offset + PERSON_SENSOR_I2C_HEADER_BYTE_COUNT

            (num_faces) = struct.unpack_from("B", read_bytes, offset)
            num_faces = int(num_faces[0])
            offset = offset + 1

            bboxes = []
            for i in range(num_faces):
                (box_confidence, box_left, box_top, box_right, box_bottom, id_confidence, id,
                 is_facing) = struct.unpack_from(PERSON_SENSOR_FACE_FORMAT, read_bytes, offset)
                offset = offset + PERSON_SENSOR_FACE_BYTE_COUNT
                if box_confidence > self.conf_threshold * 100:
                    x1 = int(box_left * self.resolution[0] / 256)
                    y1 = int(box_top * self.resolution[1] / 256)
                    x2 = int(box_right * self.resolution[0] / 256)
                    y2 = int(box_bottom * self.resolution[1] / 256)
                    bboxes.append((x1, y1, x2, y2, box_confidence))
            self.bboxes = bboxes
            checksum = struct.unpack_from("H", read_bytes, offset)
            time.sleep(PERSON_SENSOR_DELAY)

    def compute_output_frame(self):
        output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
        for x1, y1, x2, y2, conf in self.bboxes:
            cv2.putText(output, f'conf: {conf}', (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX,
                        0.3, const.COLOR_PRED, 1)
            cv2.rectangle(output, (x1, y1), (x2, y2), const.COLOR_PRED, 1)
        return cv2.resize(output, self.resolution)
