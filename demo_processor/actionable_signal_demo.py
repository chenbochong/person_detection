import numpy as np
from .base_demo import BaseDemo


class ActionableSignalDemo(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)

    def compute_output_frame(self):
        col_profile = self.emulator.read_col_profile()
        output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
        output[(self.resolution[1] - 1) - col_profile, np.arange(self.resolution[0]), :] = 255
        return output
