import cv2
import numpy as np
import timeit
import collections
import const
from .base_demo import BaseDemo


class FaceDetFullFrameDemo(BaseDemo):
    def __init__(self, emulator, power_meter, weight_path, conf_threshold=0.9):
        super().__init__(emulator, power_meter)
        self.latency_vector = collections.deque(maxlen=16)
        self.energy_vector = collections.deque(maxlen=16)
        self.detector = cv2.FaceDetectorYN.create(weight_path, "", self.resolution, conf_threshold)

    def compute_output_frame(self):
        start = timeit.default_timer()
        frame = self.emulator.read_frame()
        self.detector.setInputSize((frame.shape[1], frame.shape[0]))
        _, results = self.detector.detect(frame)
        output = frame
        for result in (results if results is not None else []):
            (x, y, w, h) = result[0:4].astype(np.int32)
            cv2.rectangle(output, (x, y), (x + w, y + h), const.COLOR_PRED, 1)

        latency_sec = timeit.default_timer() - start
        self.latency_vector.append(latency_sec * 1000)
        cv2.putText(output, f'Latency: {np.mean(self.latency_vector):.2f}ms', (10, 60),
                    cv2.FONT_HERSHEY_DUPLEX, 0.8, (126, 0, 230))
        if self.power_meter is not None:
            dynamic_power = self.power_meter.read_dynamic_watts()
            dynamic_energy = dynamic_power * latency_sec * 1000
            self.energy_vector.append(dynamic_energy)
            cv2.putText(output, f'Energy: {np.mean(self.energy_vector):.2f}mJ', (10, 90),
                        cv2.FONT_HERSHEY_DUPLEX, 0.8, (126, 0, 230))
        return output

    def stop(self):
        self.latency_vector = collections.deque(maxlen=16)
        self.energy_vector = collections.deque(maxlen=16)
