import cv2
import numpy as np

import const
from .base_demo import BaseDemo
from detector import ProfilePersonDetector


class PersonDetProfileDemo(BaseDemo):
    def __init__(self, emulator, power_meter, weight_path, conf_threshold=0.9):
        super().__init__(emulator, power_meter)
        self.mode = 'black'
        self.detector = ProfilePersonDetector(weight_path, conf_threshold=conf_threshold)

    def switch_display_mode(self, mode):
        self.mode = mode

    def compute_output_frame(self):
        col_profiles, row_profiles = self.emulator.read_grid_profiles()

        results = self.detector.detect(col_profiles, row_profiles)

        output = self._compute_canvas()
        for result in results:
            # Only display person
            if result['cls'] != 0:
                continue
            x, y, w, h = result['bbox']
            score = result['score']
            cls_name = result['cls_name']
            cv2.rectangle(output, (x, y), (x + w, y + h), const.COLOR_PRED, 1)
            cv2.putText(output, f'{cls_name}: {score:.2f}', (x, y - 10),
                        const.FONT, const.FONT_SCALE, const.COLOR_PRED, const.FONT_THICKNESS)
        return output

    def _compute_canvas(self):
        if self.mode == 'black':
            output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
        elif self.mode == 'col_profile':
            col_profile = self.emulator.read_col_profile()
            output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
            output[(self.resolution[1] - 1) - col_profile, np.arange(self.resolution[0]), :] = 255
        elif self.mode == 'actionable_signal':
            col_profile, row_profile = self.emulator.read_profile()
            output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
            output[np.arange(self.resolution[1]), row_profile, :] = 255
            output[(self.resolution[1] - 1) - col_profile, np.arange(self.resolution[0]), :] = 255
        elif self.mode == 'event_image':
            event_frame = self.emulator.read_motion_frame()
            output = cv2.cvtColor(event_frame, cv2.COLOR_GRAY2BGR)
        else:
            raise Exception(f'Person Detection: display mode {self.mode} is unknown.')
        return output
