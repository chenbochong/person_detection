import cv2
import numpy as np
import timeit
import collections
import const
from .base_demo import BaseDemo
from spu_emulator import ProfileRegionFinder, box


def check_overlap(roi_list, new_roi):
    for roi in roi_list:
        if box.intersection_over_union(roi, new_roi) > 0:
            return True
    return False


class FaceDetRoiDemo(BaseDemo):
    def __init__(self, emulator, power_meter,
                 weight_path, conf_threshold=0.9,
                 max_roi_num=7, roi_size=(128, 128),
                 use_color=True, use_motion=True):
        super().__init__(emulator, power_meter)
        self.max_roi_num = max_roi_num
        self.roi_size = roi_size
        self.use_color = use_color
        self.use_motion = use_motion
        self.region_cache = []
        self.latency_vector = collections.deque(maxlen=16)
        self.energy_vector = collections.deque(maxlen=16)
        self.detector = cv2.FaceDetectorYN.create(weight_path, "", self.resolution, conf_threshold)
        self.region_finder = ProfileRegionFinder(self.resolution)

    def stop(self):
        self.region_cache = []

    def compute_output_frame(self):
        start = timeit.default_timer()
        frame = self.emulator.read_frame()
        output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)

        # Check ROI cache
        curr_regions = []
        for x, y, w, h in self.region_cache:
            region_frame = frame[y:y+h, x:x+w, :]
            output[y:y + h, x:x + w, :] = region_frame
            self.detector.setInputSize((w, h))
            _, results = self.detector.detect(region_frame)
            if results is not None:
                for result in results:
                    x1, y1, w1, h1 = result[0:4].astype(np.int32)
                    cv2.rectangle(output, (x+x1, y+y1), (x+x1+w1, y+y1+h1), const.COLOR_PRED, 1)
                    curr_regions.append(self.result_to_roi(x+x1, y+y1, w1, h1))
        # Search for new ROIs
        if len(curr_regions) < self.max_roi_num:
            regions = self.region_finder.find_regions(self.compute_mask(), self.roi_size)
            for x, y, w, h in regions:
                if len(curr_regions) >= self.max_roi_num:
                    break
                if check_overlap(curr_regions, (x, y, w, h)):
                    continue
                region_frame = frame[y:y+h, x:x+w, :]
                output[y:y+h, x:x+w, :] = region_frame
                self.detector.setInputSize((w, h))
                _, results = self.detector.detect(region_frame)
                if results is not None:
                    for result in results:
                        x1, y1, w1, h1 = result[0:4].astype(np.int32)
                        cv2.rectangle(output, (x + x1, y + y1), (x + x1 + w1, y + y1 + h1), const.COLOR_PRED, 1)
                        curr_regions.append(self.result_to_roi(x + x1, y + y1, w1, h1))
        self.region_cache = curr_regions

        latency_sec = timeit.default_timer() - start
        self.latency_vector.append(latency_sec * 1000)
        cv2.putText(output, f'Latency: {np.mean(self.latency_vector):.2f}ms', (10, 60),
                    cv2.FONT_HERSHEY_DUPLEX, 0.8, (126, 0, 230))
        if self.power_meter is not None:
            dynamic_power = self.power_meter.read_dynamic_watts()
            dynamic_energy = dynamic_power * latency_sec * 1000
            self.energy_vector.append(dynamic_energy)
            cv2.putText(output, f'Energy: {np.mean(self.energy_vector):.2f}mJ', (10, 90),
                        cv2.FONT_HERSHEY_DUPLEX, 0.8, (126, 0, 230))
        return output

    def compute_mask(self):
        mask = np.ones((self.resolution[1], self.resolution[0]), np.uint8)
        if self.use_color:
            mask = mask & self.emulator.read_color_mask()
        if self.use_motion:
            mask = mask & self.emulator.read_motion_mask()
        return mask

    def result_to_roi(self, x, y, w, h):
        scale = 0.3
        x1 = max(0, int(x - scale / 2 * w))
        x2 = min(self.resolution[0], int(x1 + (1 + scale) * w))
        y1 = max(0, int(y - scale / 2 * h))
        y2 = min(self.resolution[1], int(y1 + (1 + scale) * h))
        return x1, y1, x2 - x1, y2 - y1
