import cv2
import numpy as np
from scipy.signal import savgol_filter, argrelmax
from scipy.signal import find_peaks
from .base_demo import BaseDemo
from spu_emulator import foreground_extraction_events, box
import const


def find_energy(profile):
    # Find the peaks in the activation compute_profile
    peaks, pk_props = find_peaks(profile, height=15, rel_height=1, width=20)

    # Iterate over the peaks and estimate the one with the most energy
    max_energy = 0
    x = 0
    w = 0
    for i in range(len(pk_props['widths'])):
        energy = pk_props['widths'][i] * pk_props['peak_heights'][i]
        if energy > max_energy:
            max_energy = energy
            x = int(pk_props['left_ips'][i])
            w = int(pk_props['widths'][i])
    return x, w


def compute_centroid(x):
    '''
    Compute the centroid of input array.
    '''
    indices = np.arange(0, x.shape[0]) + 1
    return np.sum(x * indices) // np.sum(x)


def check_overlap(roi_list, new_roi):
    '''
    Check if new ROI overlaps with list of ROI's.
    Return True if there is an overlap, else False.
    '''
    for roi in roi_list:
        # Check for no overlap between roi and new_roi
        if box.intersection_over_union(roi, new_roi) > 0:
            return True
    return False

class ForegroundExtractionDemo2(BaseDemo):
    def __init__(self, emulator, power_meter):
        super().__init__(emulator, power_meter)
        self.box_size = (200, 100)
        self.fe_mask = foreground_extraction_events.ForegroundExtractionEvents(self.resolution)

    def compute_output_frame(self):
        output = np.zeros((self.resolution[1], self.resolution[0], 3), np.uint8)
        frame = self.emulator.read_frame()
        # Get the foreground extraction smart events
        self.fe_mask.find_mask(frame)
        foreground_extraction_frame_bgr = self.fe_mask.apply_mask(frame)
        output = foreground_extraction_frame_bgr.copy()

        # Get the 1 dimentional event frame (grayscale)
        foreground_extraction_frame_gray = cv2.cvtColor(foreground_extraction_frame_bgr, cv2.COLOR_BGR2GRAY)

        col_profile = np.count_nonzero(foreground_extraction_frame_gray, axis=0)
        peaks, pk_props = find_peaks(col_profile, height=15, rel_height=1, width=50, distance=100, prominence=15)
        regions = []
        fixed_h, fixed_w = self.box_size
        rows, cols = self.resolution
        for i in range(len(peaks)):
            # Extract x and width of each peak
            x = int(pk_props['left_ips'][i])
            w = int(pk_props['widths'][i])

            # If no peak found (width of 0), then continue with next peak from column compute_profile.
            if w == 0 or w < 20:
                continue

            # Find horizontal compute_profile based only on the columns in each peak.
            row_profile = np.count_nonzero(foreground_extraction_frame_gray[:, x:x + w], axis=1)

            y, h = find_energy(row_profile)

            # If no peak found (height of 0), then continue with next peak from column compute_profile.
            if h == 0 or h < 20:
                continue

            # Filter based on ratio of width to height?
            # ratio = w/h

            # Find bounding box/region based on either the centroid of the column/row peaks or
            # based on the absolute width of the column/row peaks.
            # Find centroid of the chosen peaks
            centroid_x = compute_centroid(col_profile[x:x + w]) + x
            centroid_y = compute_centroid(row_profile[y:y + h]) + y

            x = centroid_x - fixed_w // 2
            w = fixed_w
            y = centroid_y - fixed_h // 2
            h = fixed_h

            # Resize region to be within image bounds. Keeps fixed width/height.
            (x, y, w, h) = box.check_bounds2((x, y, w, h), rows, cols)

            # Make sure ROI doesn't overlap
            if len(regions) == 0:
                regions.append((x, y, w, h))
            elif not check_overlap(regions, (x, y, w, h)):
                regions.append((x, y, w, h))
        for x, y, w, h in regions:
            cv2.rectangle(output, (x, y), (x + w, y + h), const.COLOR_PRED, 1)
        return output

    def reset_background(self):
        frame = self.emulator.read_frame()
        self.fe_mask.set_bcgrnd_frame(frame)
